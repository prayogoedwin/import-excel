<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function import_excel(){
		$this->load->library(array('excel', 'session'));
		if (isset($_FILES["fileExcel"]["name"])) {
			$path = $_FILES["fileExcel"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();	
				for($row=2; $row<=$highestRow; $row++)
				{
					$pay_nik_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$payment_date = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$rank = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$pos = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$dept = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$shop = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$plan = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
					$pay_basic_inc1 = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
					$pay_pa_inc1 = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
					$pay_ha_inc1 = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
					$pay_ih_inc1 = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
					$pay_ai_inc1 = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
					$pay_sa_inc1 = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
					$pay_ot_inc1 = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
					$pay_gas_inc1 = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
					$pay_car_inc1 = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
					$pay_tp_inc1 = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
					$pay_mealot_inc1 = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
					$pay_tpot_inc1 = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
					$pay_mealshf_inc1 = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
					$pay_leave1_inc1 = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
					$pay_itax_inc1 = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
					$pay_ir_inc1 = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
					$pay_sadj_inc1 = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
					$pay_med_inc1 = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
					$pay_donati_inc1 = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
					$pay_other1_inc1 = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
					$pay_other2_inc1 = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
					$pay_total_inc1 = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
					$pay_rbs_inc2 = $worksheet->getCellByColumnAndRow(30, $row)->getValue();
					$pay_rtarifot_inc2 = $worksheet->getCellByColumnAndRow(31, $row)->getValue();
					$pay_rmot_inc2 = $worksheet->getCellByColumnAndRow(32, $row)->getValue();
					$pay_rtot_inc2 = $worksheet->getCellByColumnAndRow(33, $row)->getValue();
					$pay_gas_inc2 = $worksheet->getCellByColumnAndRow(34, $row)->getValue();
					$pay_pos_inc2 = $worksheet->getCellByColumnAndRow(35, $row)->getValue();
					$pay_rh_inc2 = $worksheet->getCellByColumnAndRow(36, $row)->getValue();
					$pay_rthr_inc2 = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
					$pay_badm_inc2 = $worksheet->getCellByColumnAndRow(38, $row)->getValue();
					$pay_total_inc2 = $worksheet->getCellByColumnAndRow(39, $row)->getValue();
					$pay_pension_deduc = $worksheet->getCellByColumnAndRow(40, $row)->getValue();
					$pay_bpjstk_deduc = $worksheet->getCellByColumnAndRow(41, $row)->getValue();
					$pay_bpjskes_deduc = $worksheet->getCellByColumnAndRow(42, $row)->getValue();
					$pay_house_deduc = $worksheet->getCellByColumnAndRow(43, $row)->getValue();
					$pay_motor_deduc = $worksheet->getCellByColumnAndRow(44, $row)->getValue();
					$pay_car_deduc = $worksheet->getCellByColumnAndRow(45, $row)->getValue();
					$pay_medic_deduc = $worksheet->getCellByColumnAndRow(46, $row)->getValue();
					$pay_soc_deduc = $worksheet->getCellByColumnAndRow(47, $row)->getValue();
					$pay_kop_deduc = $worksheet->getCellByColumnAndRow(48, $row)->getValue();
					$pay_spmi_deduc = $worksheet->getCellByColumnAndRow(49, $row)->getValue();
					$pay_tax_deduc = $worksheet->getCellByColumnAndRow(50, $row)->getValue();
					$pay_mess_deduc = $worksheet->getCellByColumnAndRow(51, $row)->getValue();
					$pay_other1_deduc = $worksheet->getCellByColumnAndRow(52, $row)->getValue();
					$pay_other2_deduc = $worksheet->getCellByColumnAndRow(53, $row)->getValue();
					$pay_other3_deduc = $worksheet->getCellByColumnAndRow(54, $row)->getValue();
					$pay_other4_deduc = $worksheet->getCellByColumnAndRow(55, $row)->getValue();
					$pay_other5_deduc = $worksheet->getCellByColumnAndRow(57, $row)->getValue();
					$pay_other6_deduc = $worksheet->getCellByColumnAndRow(58, $row)->getValue();
					$pay_other7_deduc = $worksheet->getCellByColumnAndRow(59, $row)->getValue();
					$pay_other8_deduc = $worksheet->getCellByColumnAndRow(60, $row)->getValue();
					$total_deduc = $worksheet->getCellByColumnAndRow(61, $row)->getValue();
					$total_paid = $worksheet->getCellByColumnAndRow(62, $row)->getValue();
					$total_hours = $worksheet->getCellByColumnAndRow(63, $row)->getValue();
					$tariff1 = $worksheet->getCellByColumnAndRow(64, $row)->getValue();
					$tariff2 = $worksheet->getCellByColumnAndRow(65, $row)->getValue();
					$tariff3 = $worksheet->getCellByColumnAndRow(66, $row)->getValue();
					$tariff4 = $worksheet->getCellByColumnAndRow(67, $row)->getValue();
					$housing_loan_left = $worksheet->getCellByColumnAndRow(68, $row)->getValue();
					$motor_loan_left = $worksheet->getCellByColumnAndRow(69, $row)->getValue();
					$car_loan_left = $worksheet->getCellByColumnAndRow(70, $row)->getValue();
					$soft_loan = $worksheet->getCellByColumnAndRow(71, $row)->getValue();
					$pay_uniq_link = $worksheet->getCellByColumnAndRow(72, $row)->getValue();

					$temp_data[] = array(
						'pay_nik_id'	=> $pay_nik_id,
						'payment_date'	=> $payment_date,
						'rank'	=> $rank,
						'pos'	=> $pos,
						'dept'  => $dept,
						'shop'  => $shop,
						'plan'	=> $plan,
						'pay_basic_inc1'=>$pay_basic_inc1,
						'pay_pa_inc1'=>$pay_pa_inc1,
						'pay_ha_inc1'=>$pay_ha_inc1,
						'pay_ih_inc1'=>$pay_ih_inc1,
						'pay_ai_inc1'=>$pay_ai_inc1,
						'pay_sa_inc1'=>$pay_sa_inc1,
						'pay_ot_inc1'=>$pay_ot_inc1,
						'pay_gas_inc1'=>$pay_gas_inc1,
						'pay_car_inc1'=>$pay_car_inc1,
						'pay_tp_inc1'=>$pay_tp_inc1,
						'pay_mealot_inc1'=>$pay_mealot_inc1,
						'pay_tpot_inc1'=>$pay_tpot_inc1,
						'pay_mealshf_inc1'=>$pay_mealshf_inc1,
						'pay_leave1_inc1'=>$pay_leave1_inc1,
						'pay_itax_inc1'=>$pay_itax_inc1,
						'pay_ir_inc1'=>$pay_ir_inc1,
						'pay_sadj_inc1'=>$pay_sadj_inc1,
						'pay_med_inc1'=>$pay_med_inc1,
						'pay_donati_inc1'=>$pay_donati_inc1,
						'pay_other1_inc1'=>$pay_other1_inc1,
						'pay_other2_inc1'=>$pay_other2_inc1,
						'pay_total_inc1'=>$pay_total_inc1,
						'pay_rbs_inc2'=>$pay_rbs_inc2,
						'pay_rtarifot_inc2'=>$pay_rtarifot_inc2,
						'pay_rmot_inc2'=>$pay_rmot_inc2,
						'pay_rtot_inc2'=>$pay_rtot_inc2,
						'pay_gas_inc2'=>$pay_gas_inc2,
						'pay_pos_inc2'=>$pay_pos_inc2,
						'pay_rh_inc2'=>$pay_rh_inc2,
						'pay_rthr_inc2'=>$pay_rthr_inc2,
						'pay_badm_inc2'=>$pay_badm_inc2,
						'pay_total_inc2'=>$pay_total_inc2,
						'pay_pension_deduc'=>$pay_pension_deduc,
						'pay_bpjstk_deduc'=>$pay_bpjstk_deduc,
						'pay_bpjskes_deduc'=>$pay_bpjskes_deduc,
						'pay_house_deduc'=>$pay_house_deduc,
						'pay_motor_deduc'=>$pay_motor_deduc,
						'pay_car_deduc'=>$pay_car_deduc,
						'pay_medic_deduc'=>$pay_medic_deduc,
						'pay_soc_deduc'=>$pay_soc_deduc,
						'pay_kop_deduc'=>$pay_kop_deduc,
						'pay_spmi_deduc'=>$pay_spmi_deduc,
						'pay_tax_deduc'=>$pay_tax_deduc,
						'pay_mess_deduc'=>$pay_mess_deduc,
						'pay_other1_deduc'=>$pay_other1_deduc,
						'pay_other2_deduc'=>$pay_other2_deduc,
						'pay_other3_deduc'=>$pay_other3_deduc,
						'pay_other4_deduc'=>$pay_other4_deduc,
						'pay_other5_deduc'=>$pay_other5_deduc,
						'pay_other6_deduc'=>$pay_other6_deduc,
						'pay_other7_deduc'=>$pay_other7_deduc,
						'pay_other8_deduc'=>$pay_other8_deduc,
						'total_deduc'=>$total_deduc,
						'total_paid'=>$total_paid,
						'total_hours'=>$total_hours,
						'tariff1'=>$tariff1,
						'tariff2'=>$tariff2,
						'tariff3'=>$tariff3,
						'tariff4'=>$tariff4,
						'housing_loan_left' => $housing_loan_left,
						'motor_loan_left'=>$motor_loan_left,
						'car_loan_left'=>$car_loan_left,
						'soft_loan'=> $soft_loan,
						'pay_uniq_link'=>$pay_uniq_link


					); 	
				}
			}
			$this->load->model('Import_model');
			$insert = $this->Import_model->insert($temp_data);
			if($insert){
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
				echo "Success";
			}else{
				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-remove"></span> Terjadi Kesalahan');
				echo "Gagal";
			}
		}else{
			echo "Tidak ada file yang masuk";
		}
	}
}
